<!--
File : class_ChangeParrigin.php
Name : Justin Parrigin
Date : May 8th, 2020

-->
<?php

class Change {
	//Private Variables
	private $amountOwed;
	private $amountPaid;
	private $changeDue;
	
	//Construction for Change object
	function __construct($amountOwed, $amountPaid) {
		$this->amountOwed = $amountOwed;
		$this->amountPaid = $amountPaid;
	}
	
	//Determines the amount of change to give back
	function determine_change() {
		//Faux Values
		$fauxChangeDue = $this->changeDue;
		//Money Values Array
		$values = array(
			array("$100 Bill", "$100 Bills", 100, 0),
			array("$50 Bill", "$50 Bills", 50, 0),
			array("$20 Bill", "$20 Bills", 20, 0),
			array("$10 Bill", "$10 Bills", 10, 0),
			array("$5 Bill", "$5 Bills", 5, 0),
			array("$1 Bill", "$1 Bills", 1, 0),
			array("Quarter", "Quarters", 0.25, 0),
			array("Dime", "Dimes", 0.10, 0),
			array("Nickel", "Nickels", 0.05, 0),
			array("Penny", "Pennies", 0.01, 0)
		);
	
		for($i=0; $i<count($values); $i++) {
			//Check if the money is divisble by the values at least once
			if(($fauxChangeDue / $values[$i][2]) >= 1) {
				$amt = 0; //amount per value type
				//While the amount divided is above 1
				while(($fauxChangeDue / $values[$i][2]) >= 1) {
					$amt++;
					$fauxChangeDue = round(($fauxChangeDue - $values[$i][2]), 2);
				}
				//Set the value of array slot 3 as the amount of change
				$values[$i][3] = $amt;
				//Set the plural slot of the array
				if($amt > 1) {
					$slot = 1;
				} else {
					$slot = 0;
				}
				//Display the statement
				echo("<ul class='text-shadow'>" . $values[$i][3] . " " . $values[$i][$slot] . "</ul>");
			}
		}
	}
	
	//Get Sets for data variables
	function setAmountOwed($amountOwed) {
		$this->amountOwed = $amountOwed;
	}
	
	function setAmountPaid($amountPaid) {
		$this->amountPaid = $amountPaid;
	}
	
	function setChangeDue($changeDue) {
		$this->changeDue = $changeDue;
	}
	
	function getAmountOwed() {
		return($this->amountOwed);
	}
	
	function getAmountPaid() {
		return($this->amountPaid);
	}
	
	function getChangeDue() {
		// Sets the amount of change do from the transaction
		$this->changeDue = ($this->amountPaid - $this->amountOwed);
		return($this->changeDue);
	}
	
}

?>