<?php
?>
<!DOCTYPE html>
<!--
File : ChangeCalculator.php
Name : Justin Parrigin
Date : May 8th, 2020

-->
<html>
<head>
	<title>Calculate Change</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Font Awesome-->
    <script src="https://kit.fontawesome.com/79e102bda5.js" crossorigin="anonymous"></script>
    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Lora&display=swap" rel="stylesheet">
    <!--My Styles-->
    <link href="changecalc_styles.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <div class="container">
		<div class="back">
			<a href="http://www.justinparrigin.me#projects">Go back!</a>
		</div>
		<div class="to-repo">
			<a href="#" target="_blank">See the PHP!</a>
		</div>
        <h1 class="text-shadow">Calculate Change</h1>
        <h2 class="text-shadow">Enter transaction amount.</h2>
        <form action="ChangeCalculator.php" method="post">
            <p class="text-shadow">Amount Owed: <input type="text" name="amountOwed" placeholder="50.55"/></p>
            <p class="text-shadow">Amount Paid: <input type="text" name="amountPaid" placeholder="100.00"/></p>
            <button type="reset" name="reset"><i class="fas fa-cut text-shadow"></i></button>
            <button type="submit" name="submit"><i class="fas fa-check text-shadow"></i></button>
        </form>
        <?php
        if(isset($_POST['submit'])) {
			echo("<hr />");
            //Display new header
            echo("<h2 class='text-shadow'>Calculation Results</h2>");
            //Check if data is entered in fields
            if(empty($_POST['amountOwed']) || empty($_POST['amountPaid'])) {
                echo("<p class='text-shadow'>You must enter data for both fields.</p>");	
            //Check if the data entered is numeric
            } else if(!is_numeric($_POST['amountOwed']) || !is_numeric($_POST['amountPaid'])) {
                echo("<p class='text-shadow'>You must enter valid amounts for both fields.</p>");
            } else if(!($_POST['amountOwed'] < $_POST['amountPaid'])) {
				echo("<p class='text-shadow'>Insufficient funds provided.</p>");
			} else {
            //If data is cleared, create new Change object and process data
                include("class_ChangeParrigin.php");
                $Change = new Change($_POST['amountOwed'], $_POST['amountPaid']);
            }
            //Checks if object is made
            if(isset($Change)) {
                //Display price of transaction
                printf("<p class='text-shadow'>The price of the transaction was $%.2f.</p>", $Change->getAmountOwed());
                //Display the amount paid
                printf("<p class='text-shadow'>The amount paid was $%.2f.</p>", $Change->getAmountPaid());
                //Display change due
                printf("<p class='text-shadow'>The change due is $%.2f.</p>", $Change->getChangeDue());
                //Display the type of change to return
                echo("<p class='text-shadow'>Return the following denominations as change:</p>");

                $Change->determine_change();
            }
        }
        ?>
    </div>
<body>
</html>